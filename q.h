

#include <queue>
#include <mutex>
//#include <synchapi.h>
using std::mutex;
using std::pair;

 
template <class T>
class Queue {
	mutex m;
	std::queue<T> queue;
public:
	 

	void InsertData(const T& data) {
		m.lock();
		queue.push(data);
		m.unlock();
	}

	pair<T, T> GetData() {
		m.lock();
		if (queue.size() < 2) {
			m.unlock();
			throw std::exception();
			// return pair<T, T>(T(), T());
		}
		T ret1 = queue.front();
		queue.pop();
		T ret2 = queue.front();
		queue.pop();
		m.unlock();
		return pair<T, T>(ret1, ret2);
	}

	T front() {
		return queue.front();
	}
};
