#include <iostream>
#include <thread>
#include <vector>
#include <queue>
#include "q.h"

using std::queue;
using std::thread;
using std::vector;
using std::pair;
using std::cout;

void print(vector<int> v)
{
	for (int i = 0; i < v.size(); i++) {
		cout << v[i] << " ";
	}
	cout << std::endl;
}

vector<int> Merge(vector<int> left, vector<int> right)
{
	vector<int> result;
	while ((int)left.size() > 0 || (int)right.size() > 0) {
		if ((int)left.size() > 0 && (int)right.size() > 0) {
			if ((int)left.front() <= (int)right.front()) {
				result.push_back((int)left.front());
				left.erase(left.begin());
			}
			else {
				result.push_back((int)right.front());
				right.erase(right.begin());
			}
		}
		else if ((int)left.size() > 0) {
			for (int i = 0; i < (int)left.size(); i++)
				result.push_back(left[i]);
			break;
		}
		else if ((int)right.size() > 0) {
			for (int i = 0; i < (int)right.size(); i++)
				result.push_back(right[i]);
			break;
		}
	}
	return result;
}

void merge(Queue<vector<int>>* queue) {
	while (true) {
		try {
			pair<vector<int>, vector<int>> v = queue->GetData();

			vector<int> x = Merge(v.first, v.second);

			queue->InsertData(x);
		}
		catch (const std::exception&) {
			break;
		}
	}
}

class Threadpool {
	Queue<vector<int>> queue;
	vector<thread> threads;

public:
	Threadpool(int number, vector<int> v) {
		for (int x : v) {
			queue.InsertData(vector<int>({ x }));
		}

		for (int i = 0; i < number; i++) {
			threads.push_back(thread(merge, &queue));
		}
	}

	void join() {
		for (int i = 0; i < threads.size(); i++) {
			threads[i].join();
		}

	}

	void add(vector<int> v) {
		queue.InsertData(v);
	}

	vector<int> get() {
		return queue.front();
	}
};

int main() {
	vector<int> a({ 90, 80, 30, 44, 12, 99, 48, 11, 77 });
	Threadpool tp(4, a);
	
	tp.join();

	vector<int> result = tp.get();
	for (int x : result) {
		cout << x << " ";
	}

	return 0;
}
